/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 54);
/******/ })
/************************************************************************/
/******/ ({

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(55);


/***/ }),

/***/ 55:
/***/ (function(module, exports) {

document.addEventListener('DOMContentLoaded', function (event) {
  var mirrors = [];
  var currentMirror = 0;
  var lastMirror = getCookie('last-mirror-host');
  console.log('Last mirror was: ' + lastMirror);
  var getMirrors = new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/redirector/mirrors');
    xhr.onload = function () {
      if (xhr.status === 200) {
        resolve(xhr.response);
      } else {
        reject();
      }
    };
    xhr.send();
  });

  getMirrors.then(function (result) {
    mirrors = JSON.parse(result);
    console.log('Mirrors loaded');
  }).then(function () {
    checkNextMirror();
  });

  function checkNextMirror() {
    var img = new Image();
    var scheme = mirrors[currentMirror].is_ssl ? 'https://' : 'http://';
    img.onload = function () {
      logRedirect(mirrors[currentMirror].host);
      remember(mirrors[currentMirror]);
      window.location.href = scheme + mirrors[currentMirror].host + window.location.pathname + window.location.search + window.location.hash;
    };
    img.onerror = function (e) {
      logFailure(mirrors[currentMirror].host);
      if (currentMirror === mirrors.length - 1) {
        if (Redirector.show_error_message) {
          document.getElementById('loader').innerHTML = '';
          document.getElementById('message').style.display = 'block';
        }
      } else {
        currentMirror = currentMirror + 1;
        checkNextMirror();
      }
    };
    img.src = scheme + mirrors[currentMirror].host + '/' + Redirector.status_file + '?t=' + Date.now();
  }

  function remember(mirror) {
    setCookie('last-mirror-host', mirror.host, 90);
    setCookie('last-mirror-ssl', mirror.is_ssl, 90);
    if (mirror.country) {
      setCookie('last-mirror-country', mirror.country, 90);
    }
  }

  function logFailure(host) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/redirector/log/error');
    var token = document.head.querySelector('meta[name="csrf-token"]');
    xhr.setRequestHeader('X-CSRF-TOKEN', token.content);
    var formData = new FormData();
    formData.set('last_available', lastMirror);
    formData.set('last_tried', host);
    formData.set('referrer', document.referrer);
    formData.set('uri', window.location.pathname + window.location.search + window.location.hash);
    xhr.send(formData);
  }

  function logRedirect(host) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/redirector/log/redirect');
    var token = document.head.querySelector('meta[name="csrf-token"]');
    xhr.setRequestHeader('X-CSRF-TOKEN', token.content);
    var formData = new FormData();
    formData.set('mirror', host);
    formData.set('referrer', document.referrer);
    formData.set('uri', window.location.pathname + window.location.search + window.location.hash);
    xhr.send(formData);
  }

  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  }

  function getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }

    return '';
  }
});

/***/ })

/******/ });