# Редиректор

Гейт для поиска первого доступного зеркала и редиректа

## Требования

- PHP 7.1.3+
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Sqlite PHP Extension
- Intl PHP Extension

## Установка

```
composer install
```

- Скопировать файл `.env.example` в `.env`
- Обновить строки в `.env`
    ```
    DB_CONNECTION=sqlite
    DB_DATABASE=database.sqlite
    SUPPORT_EMAIL=<email тех.поддержки>
    STATUS_FILE=<файл для проверки доступности>
    ```
- Выполнить более точную настройку можно в файле `.env`, описание параметров содержится в нем
- Создать файл `database/database.sqlite` и дать права на запись для приложения
- Дать права на запись для приложения на все в папке `storage`
- Выполнить команды
    ```
    php artisan key:generate
    php artisan install -U <email_админа> -P <пароль_админа> --force
    ```

Последняя команда запустит процесс создания нового пользователя 

Точка входа в приложение: `public/index.php`

## Требования к зеркалам

В корне каждого зеркала должен быть файл magic.gif

## Админка

```
/admin
```

## Обновление базы GeoIP

* Скачать базу стран и положить ее в папку storage
* В `.env` указать имя нового файла
* **Не рекомендуется** перезаписывать файл по-умолчанию, который поставляется с сервисом
