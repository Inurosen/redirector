<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mirror extends Model
{
    protected $table = 'mirrors';
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ['host', 'is_enabled', 'is_ssl', 'created_at', 'country'];
    protected $casts = [
        'is_enabled' => 'int',
        'is_ssl'     => 'int',
    ];
}
