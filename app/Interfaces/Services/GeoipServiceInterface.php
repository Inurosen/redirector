<?php

namespace App\Interfaces\Services;

interface GeoipServiceInterface
{
    public function getCountry(string $ip);
}
