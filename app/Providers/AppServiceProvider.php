<?php

namespace App\Providers;

use App\Interfaces\Services\GeoipServiceInterface;
use App\Services\GeoipService;
use Illuminate\Support\ServiceProvider;
use MaxMind\Db\Reader;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GeoipServiceInterface::class, function () {
            return new GeoipService(new Reader(
                storage_path(env('GEOIP_COUNTRY_DB_FILE'))
            ));
        });
    }
}
