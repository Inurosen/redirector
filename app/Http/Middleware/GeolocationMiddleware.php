<?php

namespace App\Http\Middleware;

use App\Interfaces\Services\GeoipServiceInterface;
use Closure;
use Illuminate\Support\Facades\Config;

class GeolocationMiddleware
{
    /**
     * @var GeoipServiceInterface
     */
    private $geoipService;

    public function __construct(GeoipServiceInterface $geoipService)
    {
        $this->geoipService = $geoipService;
    }

    public function handle($request, Closure $next)
    {
        $country = $this->geoipService->getCountry($request->getClientIp());
        if ($country) {
            Config::set('app.locale', $this->getLanguage($country));
            Config::set('app.country', $country);
        }

        return $next($request);
    }

    function getLanguage(string $country): string
    {
        $subtags = \ResourceBundle::create('likelySubtags', 'ICUDATA', false);
        $country = \Locale::canonicalize('und_' . $country);
        $locale = $subtags->get($country) ?: $subtags->get('und');

        return \Locale::getPrimaryLanguage($locale);
    }
}
