<?php

namespace App\Http\Controllers;

use App\Mirror;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function mirrors(Request $request)
    {
        $country = config('app.country');
        $lastMirror = $request->cookie('last-mirror-host');
        $lastMirrorSsl = $request->cookie('last-mirror-ssl', 0);
        $lastMirrorCountry = $request->cookie('last-mirror-country');

        $mirrors = new Collection();
        if (!empty($lastMirror)) {
            $mirrors->add(['host' => $lastMirror, 'is_ssl' => $lastMirrorSsl, 'country' => $lastMirrorCountry]);
        }
        $geoMirrors = new \Illuminate\Support\Collection();
        if ($country) {
            $geoMirrors = Mirror::where('is_enabled', 1)
                ->where('country', $country)
                ->get(['host', 'is_ssl', 'country']);
            if ($geoMirrors) {
                $mirrors = $mirrors->concat($geoMirrors);
            }
        }
        if ($geoMirrors->count() === 0) {
            $commonMirrors = Mirror::where('is_enabled', 1)
                ->whereNull('country')
                ->get(['host', 'is_ssl', 'country']);
            if ($commonMirrors) {
                $mirrors = $mirrors->concat($commonMirrors);
            }
        }

        return $mirrors;
    }

    public function addErrorLog(Request $request)
    {
        $fileName = storage_path('logs') . DIRECTORY_SEPARATOR . 'failed_mirrors.log';
        $this->prependToFile($fileName, json_encode([
                'timestamp'      => time(),
                'last_available' => $request->input('last_available'),
                'last_tried'     => $request->input('last_tried'),
                'referrer'       => $request->input('referrer'),
                'uri'            => $request->input('uri'),
                'ip'             => $request->ip(),
                'country'        => config('app.country'),
            ], JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);

        return $request->input();
    }

    public function addRedirectLog(Request $request)
    {
        $fileName = storage_path('logs') . DIRECTORY_SEPARATOR . 'redirected_mirrors.log';
        $this->prependToFile($fileName, json_encode([
                'timestamp' => time(),
                'mirror'    => $request->input('mirror'),
                'referrer'  => $request->input('referrer'),
                'uri'       => $request->input('uri'),
                'ip'        => $request->ip(),
                'country'   => config('app.country'),
            ], JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);

        return $request->input();
    }

    private function prependToFile($fileName, $data)
    {
        if (!file_exists($fileName)) {
            touch($fileName);
        }

        $file = fopen($fileName, 'r+');
        $temp = fopen('php://temp', 'w');

        fwrite($temp, $data);

        stream_copy_to_stream($file, $temp);
        rewind($temp);
        rewind($file);
        stream_copy_to_stream($temp, $file);

        fclose($file);
        fclose($temp);
    }
}
