<?php

namespace App\Http\Controllers;

use App\Http\Requests\MirrorRequest;
use App\Mirror;
use Carbon\Carbon;
use Illuminate\Http\Request;
use SplFileObject;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect(route('admin.mirrors'));
    }

    public function mirrors(Request $request)
    {
        if (!$request->ajax()) {
            return view('mirrors');
        }

        $mirrors = Mirror::all();

        return response()->json(['success' => true, 'data' => $mirrors,]);
    }


    public function errors(Request $request)
    {
        if ($request->wantsJson()) {
            $offset = $request->get('offset', 0);
            $i = 0;
            $fileName = storage_path('logs') . DIRECTORY_SEPARATOR . 'failed_mirrors.log';
            $errors = [];
            if (file_exists($fileName)) {
                $file = new SplFileObject($fileName);
                if ($file) {
                    for ($i = 0; $i < 100; $i++) {
                        $file->seek($i + $offset);
                        $line = $file->current();
                        if ($line === false) {
                            break;
                        }
                        $json = json_decode($line, true);
                        if (!empty($json)) {
                            $errors[] = $json;
                        }
                    }
                }
            }

            rsort($errors);

            return response()->json(['success' => true, 'offset' => $offset + $i, 'data' => $errors,]);
        }


        return view('errors');
    }

    public function redirects(Request $request)
    {
        if ($request->wantsJson()) {
            $offset = $request->get('offset', 0);
            $i = 0;
            $fileName = storage_path('logs') . DIRECTORY_SEPARATOR . 'redirected_mirrors.log';
            $redirects = [];
            if (file_exists($fileName)) {
                $file = new SplFileObject($fileName);
                if ($file) {
                    for ($i = 0; $i < 100; $i++) {
                        $file->seek($i + $offset);
                        $line = $file->current();
                        if ($line === false) {
                            break;
                        }
                        $json = json_decode($line, true);
                        if (!empty($json)) {
                            $redirects[] = $json;
                        }
                    }
                }
            }

            rsort($redirects);

            return response()->json(['success' => true, 'offset' => $offset + $i, 'data' => $redirects,]);
        }

        return view('redirects');
    }

    public function clearErrors()
    {
        $fileName = storage_path('logs') . DIRECTORY_SEPARATOR . 'failed_mirrors.log';
        unlink($fileName);

        return ['success' => true];
    }

    public function clearRedirects()
    {
        $fileName = storage_path('logs') . DIRECTORY_SEPARATOR . 'redirected_mirrors.log';
        unlink($fileName);

        return ['success' => true];
    }

    public function create(MirrorRequest $request)
    {
        try {
            $host = $request->input('host');
            $matches = [];
            if (!preg_match('/^(https?:\/\/)?([^\/]+)?.*$/', $host, $matches)) {
                throw new \Exception('Не удалось извлечь хост из указанной ссылки');
            }
            $host = $matches[2];
            $isEnabled = $request->input('is_enabled', false);
            $isSsl = $request->input('is_ssl', false);
            $country = $request->input('country', null);
            $createdAt = Carbon::now();

            $mirror = Mirror::create([
                'host'       => $host,
                'country'    => $country ? mb_strtoupper($country) : null,
                'is_enabled' => $isEnabled,
                'is_ssl'     => $isSsl,
                'created_at' => $createdAt,
            ]);
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'errors' => $exception->getMessage()], 500);
        }

        return response()->json(['success' => true, 'data' => $mirror]);
    }

    public function delete(Request $request, $id)
    {
        Mirror::where('id', $id)->delete();

        return response()->json(['success' => true]);
    }

    public function update(Request $request, $id)
    {
        try {
            $host = $request->input('host');
            $matches = [];
            if (!preg_match('/^(https?:\/\/)?([^\/]+)?.*$/', $host, $matches)) {
                throw new \Exception('Не удалось извлечь хост из указанной ссылки');
            }
            $host = $matches[2];
            $isEnabled = $request->input('is_enabled', false);
            $isSsl = $request->input('is_ssl', false);
            $country = $request->input('country');
            $updatedAt = Carbon::now();
            if (Mirror::where('id', '!=', $id)->where('host', $host)->where('country', $country)->count()) {
                return response()->json(['success' => false, 'errors' => ['host' => ['Такое зеркало уже существует']]],
                    400);
            }

            $mirror = Mirror::findOrFail($id);
            $mirror->host = $host;
            $mirror->is_enabled = $isEnabled;
            $mirror->country = $country ? mb_strtoupper($country) : null;
            $mirror->is_ssl = $isSsl;
            $mirror->updated_at = $updatedAt;
            $mirror->save();
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'errors' => $exception->getMessage()], 500);
        }

        return response()->json(['success' => true, 'data' => $mirror]);
    }
}
