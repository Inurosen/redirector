<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install {--U|user=} {--P|password=} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выполняет установку приложения';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $force = $this->option('force');
        if (!$force) {
            if (!$this->confirm('Все данные будут утеряны. Вы уверены?')) {
                return false;
            }
        }
        $this->info('Установка...');
        $this->call('migrate:fresh', ['--force' => true]);

        $user = $this->getUser();
        $password = $this->getPass();

        User::create([
            'name'     => $user,
            'email'    => $user,
            'password' => Hash::make($password),
        ]);
        $this->info('Пользователь ' . $user . ' создан');

        return true;
    }

    protected function getUser()
    {
        $user = $this->option('user');
        if (!$user) {
            $this->warn('Необходимо указать email нового пользователя');
            $user = $this->ask('Email:');
            $this->getUser();
        }

        return $user;
    }

    protected function getPass()
    {
        $pass = $this->option('password');
        if (!$pass) {
            $this->warn('Необходимо указать нового пользователя');
            $pass = $this->ask('Пароль:');
            $this->getPass();
        }

        return $pass;
    }
}
