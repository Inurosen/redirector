<?php


namespace App\Services;


use App\Interfaces\Services\GeoipServiceInterface;
use Illuminate\Support\Arr;
use MaxMind\Db\Reader;

class GeoipService implements GeoipServiceInterface
{
    private $reader;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function getCountry(string $ip)
    {
        return Arr::get($this->reader->get($ip) ?? [], 'country.iso_code');
    }

    public function __destruct()
    {
        $this->reader->close();
    }
}
