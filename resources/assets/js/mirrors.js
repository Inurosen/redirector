document.addEventListener('DOMContentLoaded', function (event) {
  let mirrors = []
  let currentMirror = 0
  let lastMirror = getCookie('last-mirror-host')
  console.log('Last mirror was: ' + lastMirror)
  let getMirrors = new Promise(function (resolve, reject) {
    let xhr = new XMLHttpRequest()
    xhr.open('GET', '/redirector/mirrors')
    xhr.onload = function () {
      if (xhr.status === 200) {
        resolve(xhr.response)
      } else {
        reject()
      }
    }
    xhr.send()
  })

  getMirrors.then(function (result) {
    mirrors = JSON.parse(result)
    console.log('Mirrors loaded')
  }).then(function () {
    checkNextMirror()
  })

  function checkNextMirror () {
    let img = new Image()
    let scheme = mirrors[currentMirror].is_ssl ? 'https://' : 'http://'
    img.onload = function () {
      logRedirect(mirrors[currentMirror].host)
      remember(mirrors[currentMirror])
      window.location.href = scheme + mirrors[currentMirror].host + window.location.pathname + window.location.search + window.location.hash
    }
    img.onerror = function (e) {
      logFailure(mirrors[currentMirror].host)
      if (currentMirror === mirrors.length - 1) {
        if (Redirector.show_error_message) {
          document.getElementById('loader').innerHTML = ''
          document.getElementById('message').style.display = 'block'
        }
      } else {
        currentMirror = currentMirror + 1
        checkNextMirror()
      }
    }
    img.src = scheme + mirrors[currentMirror].host + '/' + Redirector.status_file + '?t=' + Date.now()
  }

  function remember (mirror) {
    setCookie('last-mirror-host', mirror.host, 90)
    setCookie('last-mirror-ssl', mirror.is_ssl, 90)
    if (mirror.country) {
      setCookie('last-mirror-country', mirror.country, 90)
    }
  }

  function logFailure (host) {
    let xhr = new XMLHttpRequest()
    xhr.open('POST', '/redirector/log/error')
    let token = document.head.querySelector('meta[name="csrf-token"]')
    xhr.setRequestHeader('X-CSRF-TOKEN', token.content)
    let formData = new FormData()
    formData.set('last_available', lastMirror)
    formData.set('last_tried', host)
    formData.set('referrer', document.referrer)
    formData.set('uri', window.location.pathname + window.location.search + window.location.hash)
    xhr.send(formData)
  }

  function logRedirect (host) {
    let xhr = new XMLHttpRequest()
    xhr.open('POST', '/redirector/log/redirect')
    let token = document.head.querySelector('meta[name="csrf-token"]')
    xhr.setRequestHeader('X-CSRF-TOKEN', token.content)
    let formData = new FormData()
    formData.set('mirror', host)
    formData.set('referrer', document.referrer)
    formData.set('uri', window.location.pathname + window.location.search + window.location.hash)
    xhr.send(formData)
  }

  function setCookie (cname, cvalue, exdays) {
    let d = new Date()
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000))
    let expires = 'expires=' + d.toUTCString()
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'
  }

  function getCookie (cname) {
    let name = cname + '='
    let ca = document.cookie.split(';')
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i]
      while (c.charAt(0) === ' ') {
        c = c.substring(1)
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length)
      }
    }

    return ''
  }

})

