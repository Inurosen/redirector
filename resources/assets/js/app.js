/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.eventBus = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('mirrors-create', require('./components/MirrorsCreate.vue'));
Vue.component('mirrors-list', require('./components/MirrorsList.vue'));
Vue.component('errors-list', require('./components/ErrorsList.vue'));
Vue.component('redirects-list', require('./components/RedirectsList'));

const app = new Vue({
  el: '#app'
});
