@extends('layouts.admin')

@section('content')
    <div class="container">
        <mirrors-create></mirrors-create>
        <mirrors-list></mirrors-list>
    </div>
@endsection
