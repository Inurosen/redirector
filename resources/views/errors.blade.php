@extends('layouts.admin')

@section('content')
    <div class="container">
        <errors-list :errors="[]"></errors-list>
    </div>
@endsection
