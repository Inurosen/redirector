<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! env('PAGE_TITLE') !!}</title>
    <meta name="description" content="{{ env('PAGE_DESCRIPTION') }}"/>
    <meta name="keywords" content="{{ env('PAGE_KEYWORDS') }}"/>

    @if(env('PAGE_FAVICON_16'))
        <link rel="icon" type="image/png" sizes="16x16" href="{!! env('PAGE_FAVICON_16') !!}">
    @endif
    @if(env('PAGE_FAVICON_32'))
        <link rel="icon" type="image/png" sizes="32x32" href="{!! env('PAGE_FAVICON_32') !!}">
    @endif

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
</head>
<body style="margin:0;">
<div>
    <div style="width: 100%; height: 100%; display: block; position: fixed; background-color: #{{ env('PAGE_BACKGROUD_COLOR', '000') }}; z-index: 1000000;"
         class="ng-scope">
        <style>
            .loading {
                position: absolute;
                top: 50%;
                left: 50%;
            }

            .loading .bullet {
                position: absolute;
                padding: 5px;
                border-radius: 50%;
                background: #{{ env('PAGE_SPINNER_COLOR', 'd63') }};
                -webkit-animation: animIn 1s ease-in-out 0s infinite;
                animation: animIn 1s ease-in-out 0s infinite;
            }

            .loading .bullet:nth-child(1) {
                -webkit-animation-delay: 0s;
                animation-delay: 0s;
            }

            .loading .bullet:nth-child(2) {
                -webkit-animation-delay: 0.15s;
                animation-delay: 0.15s;
            }

            .loading .bullet:nth-child(3) {
                -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
            }

            @-webkit-keyframes animIn {
                0% {
                    -webkit-transform: translateX(-100px);
                    transform: translateX(-100px);
                    opacity: 0;
                }
                50% {
                    opacity: 1;
                }
                100% {
                    -webkit-transform: translateX(100px);
                    transform: translateX(100px);
                    opacity: 0;
                }
            }

            @keyframes animIn {
                0% {
                    -webkit-transform: translateX(-100px);
                    transform: translateX(-100px);
                    opacity: 0;
                }
                50% {
                    opacity: 1;
                }
                100% {
                    -webkit-transform: translateX(100px);
                    transform: translateX(100px);
                    opacity: 0;
                }
            }

            #logo {
                width: 160px;
                left: calc(50% - 160px / 2);
                top: calc(50% - 200px);
                margin: auto;
                text-align: center;
                position: absolute;
            }

            #logo img {
                width: inherit;
                height: inherit;
            }

            #message {
                margin: {{ env('PAGE_LOGO') ? '50vh' : '40vh' }} auto 0;
                max-width: 700px;
                color: #{{ env('PAGE_TEXT_COLOR', 'fff') }};
                font-size: 24px;
                font-family: 'Roboto', sans-serif;
                font-weight: 300;
                line-height: 36px;
                text-align: center;
                display: none;
            }

            #message a {
                color: #{{ env('PAGE_LINK_COLOR', 'd63') }};
            }

            #message a:hover {
                text-decoration: none;
            }
        </style>
        @if(env('PAGE_LOGO'))
            <div id="logo">
                <img src="{!! env('PAGE_LOGO') !!}" alt="Logo">
            </div>
        @endif
        @if(env('SHOW_ERROR_MESSAGE'))
            <div id="message">
                {!! trans('redirector.mirrors.error', ['email' => env('SUPPORT_EMAIL')]) !!}
            </div>
        @endif
        <div id="loader" class="loading">
            <div class="bullet"></div>
            <div class="bullet"></div>
            <div class="bullet"></div>
        </div>
    </div>
</div>
<script>
  let Redirector = {
    status_file: '{{ env('STATUS_FILE') }}',
    show_error_message: {{ env('SHOW_ERROR_MESSAGE') ? 'true' : 'false' }}
  }
</script>
<script type="text/javascript" src="{{ asset('js/mirrors.js') }}"></script>
</body>
</html>
