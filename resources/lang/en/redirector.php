<?php

return [
    'mirrors' => [
        'error' => 'Unfortunately, we couldn\'t find an available mirror site for you at the moment. Please contact the customer support via <a href="mailto::email">:email</a> to get additional information.',
    ],
];
