<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\GeolocationMiddleware;

Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@login')->name('admin.login');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth'], function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::get('/mirrors', 'AdminController@mirrors')->name('mirrors');
    Route::post('/mirrors', 'AdminController@create')->name('mirrors.create');
    Route::post('/mirrors/{id}', 'AdminController@update')->name('mirrors.update');
    Route::delete('/mirrors/{id}', 'AdminController@delete')->name('mirrors.update');

    Route::get('/errors', 'AdminController@errors')->name('errors');
    Route::post('/errors', 'AdminController@clearErrors')->name('errors.delete');

    Route::get('/redirects', 'AdminController@redirects')->name('redirects');
    Route::post('/redirects', 'AdminController@clearRedirects')->name('redirects.delete');

    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
});
Route::group(['prefix' => 'redirector', 'as' => 'redirector.', 'middleware' => GeolocationMiddleware::class],
    function () {
        Route::get('/mirrors', 'RedirectController@mirrors')->name('mirrors');
        Route::post('/log/error', 'RedirectController@addErrorLog')->name('log.error.add');
        Route::post('/log/redirect', 'RedirectController@addRedirectLog')->name('log.redirect.add');
    });

Route::get('/{path?}', 'RedirectController@index')->where('path', '.*')->middleware(GeolocationMiddleware::class);
